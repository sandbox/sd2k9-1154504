Drupal foswikifilter.module README.txt
================================================================================

The drupal foswikifilter module adds a foswiki filter to Drupal.
This custom content filter formats nodes using Foswiki style formatting.
It is under development, but should be already usable (at your own risk)

Attention: A lot of formatting options do not work yet



Installation
------------------------------------------------------------------------------
 
  - Download the Foswikifilter module from http://drupal.org/sandbox/sd2k9/1154504

  - Create a .../modules/foswikifilter/ subdirectory and copy the files into it.

  - Enable the module as usual from Drupal's admin pages
    (Administer » Modules)

  - Before using the foswiki syntax you need to create an foswiki filter
    in an input format
    - Go to Administer » Configuration » text formats
    - Add text format (e.g. "Foswiki Filter")
    - Enable "Foswiki filter " as filter for this format


Supported Foswiki Syntax
------------------------------------------------------------------------------

See module edit help (when you create a topic with foswiki filter then
the help screen is linked).


Credits / Contacts
------------------------------------------------------------------------------


Original work done by mradcliffe for twikifilter
	 http://drupal.org/project/twikifilter

Migration to Drupal 7 and renaming to foswikifilter done by sd2k9
	  http://drupal.org/sandbox/sd2k9/1154504
